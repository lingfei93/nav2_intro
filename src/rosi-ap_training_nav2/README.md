**Install ROS2 Foxy in Ubuntu 20.04**
``` 
  Please refer to https://docs.ros.org/en/foxy/index.html
```

**Install Navigation2**
```
  Please refer to https://navigation.ros.org/index.html
  (It will install required dependencies automatically)
```


**Clone This Repo**
```
  git clone git@gitlab.com:ROSI-AP/rosi-ap_training_nav2.git
```

**Compile This Repo**
```
  colcon build
```

**Start Gazebo**
```
  source /opt/ros/foxy/setup.bash
  source install/setup.sh
  export TURTLEBOT3_MODEL=waffle
  export GAZEBO_MODEL_PATH=<path>/turtlebot3-2.1.1/turtlebot3_simulations/turtlebot3_gazebo/models
  ros2 launch turtlebot3_gazebo turtlebot3_world.launch.py
```


**Start Rviz** (new terminal)
```
  source /opt/ros/foxy/setup.bash
  source install/setup.sh
  ros2 param set /gazebo use_sim_tim True
  ros2 launch turtlebot3_cartographer cartographer.launch.py use_sim_time:=true
```

  
**Start teleop_keyboard** (new terminal)
 ```
  source /opt/ros/foxy/setup.bash
  source install/setup.sh
  export TURTLEBOT3_MODEL=waffle
  ros2 run turtlebot3_teleop teleop_keyboard
```

